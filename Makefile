#
## RHD (all build logic now in hand written build.xml)
#

.PHONY: build test clean install installsys

build:
	(cd lib; make build)
	ant

test:
	(cd lib; make build)
	ant test

install: dist/bin/dicomrouter 
	mkdir -p /usr/local/dcmrouter
	cp -a dist/* /usr/local/dcmrouter
	mkdir -p /usr/local/dcmrouter/logs

installsys:
	sudo cp etc/init.d/dicomrouter /etc/init.d/
	sudo cp etc/cron.d/dicomrouter /etc/cron.d/

clean:
	ant clean
	rm -f TEST-de.*.txt
	(cd lib; make clean)
