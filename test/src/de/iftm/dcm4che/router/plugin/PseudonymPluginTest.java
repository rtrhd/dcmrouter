/*
 *  UnitTest: PseudonymPluginTest.java
 *
 *  Copyright (c) 2003 by
 *
 *  VISUS Technology Transfer GmbH
 *  IFTM Institut fuer Telematik in der Medizn GmbH
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 *****************************************************************************/
package de.iftm.dcm4che.router.plugin;

import junit.framework.*;

import org.apache.log4j.*;

import org.dcm4che.data.*;

import org.dcm4che.dict.*;

import java.util.*;


/**
 * JUnit based test for the class: PseudonymPlugin.java
 * - Modified RHD 02/02/2012
 * <br>
 * @author Thomas Hacklaender
 * @version 2006.11.17
 */
public class PseudonymPluginTest extends TestCase {

    /** A link to a MR DICOM file */
    final String MR_IMAGE_URI = "./example_images/MR_example.dcm";

    /** A link to a Siemens ASL MR DICOM file */
    final String MR_ASL_SIEMENS_IMAGE_URI = "./example_images/MR_asl_siemens_example.dcm";

    /** A link to a Siemens fMRI MR DICOM file */
    final String MR_FNL_SIEMENS_IMAGE_URI = "./example_images/MR_fnl_siemens_example.dcm";

    /** A link to a Siemens DTI MR DICOM file */
    final String MR_DTI_SIEMENS_IMAGE_URI = "./example_images/MR_dti_siemens_example.dcm";

    /** A link to a Siemens DTI MRS file */
    final String MRS_SIEMENS_IMAGE_URI = "./example_images/MRS_siemens_example.dcm";
    
    /** A link to a CT DICOM file */
    final String CT_IMAGE_URI = "./example_images/CT_example.dcm";

    /** A link to a CR DICOM file */
    final String CR_IMAGE_URI = "./example_images/CR_example.dcm";

    /** The class to test */
    PseudonymPlugin pseudonymPlugin = null;

    /**
     * Creates a new PseudonymPluginTest object.
     *
     * @param testName DOCUMENT ME!
     */
    public PseudonymPluginTest(java.lang.String testName) {
        super(testName);
    }

    /**
     * Main method to run the test suite.
     *
     * @param args The argument strings.
     */
    public static void main(java.lang.String[] args) {
        junit.textui.TestRunner.run(suite());
    }


    /**
     * Create a new test suite based on this class.
     *
     * @return The test suite
     */
    public static Test suite() {
        TestSuite suite = new TestSuite(PseudonymPluginTest.class);

        return suite;
    }


    /**
     * Setup procedures.
     * This method is called once during initialization.
     */
    protected void setUp() {
        // Delete all existing appenders and set logging level to INFO
        BasicConfigurator.resetConfiguration();

        // Initialise Logging System 
        BasicConfigurator.configure();

        // Set Logging Level to INFO
        Logger.getRootLogger().setLevel(Level.INFO);

        // Create an instance of the plugin
        pseudonymPlugin = new PseudonymPlugin();

        // Set empty properties for the plugin
        pseudonymPlugin.init(new Properties());
    }


    /**
     * Cleanup procedures.
     * This method is called once before finishing the test suite.
     */
    protected void tearDown() {
        // Nothing to do
    }


    /**
     * Test of process method for a patient scan,
     * of class de.iftm.dcm4che.router.plugin.PseudonymPlugin.
     */
    public void testProcessPatient() {
        System.out.println("testProcessPatient");
        Dataset ds = DcmObjectFactory.getInstance().newDataset();
    }



    /**
     * Test of initials2_Birdat6 method,
     *  of class de.iftm.dcm4che.router.plugin.PseudonymPlugin.
     */
    public void testInitials2_Birdat6() {
        Dataset ds = null;
        String  s;

        System.out.println("testInitials2_Birdat6");

        // Create an empty dataset
        ds = DcmObjectFactory.getInstance().newDataset();

        // Set the character set to ISO-8859-1 for Western Europe
        // Otherwise we would get an IllegalArgumentException on loading
        // a dataset with German umlauts 
        ds.putCS(Tags.SpecificCharacterSet, "ISO_IR 100");

        s = pseudonymPlugin.initials2_Birdat6(ds);
        assertEquals(s, "XX_000101", s);

        ds.putDA(Tags.PatientBirthDate, "19401201");
        s = pseudonymPlugin.initials2_Birdat6 (ds);
        assertEquals(s, "XX_401201", s);

        ds.putPN(Tags.PatientName, "Mustermann^Hans Werner^von^Dr.");
        ds.putDA(Tags.PatientBirthDate, "19401201");
        s = pseudonymPlugin.initials2_Birdat6(ds);
        assertEquals(s, "MH_401201", s);
    }


    /**
     * Test of mod2Acqdat6_Birdat6 method, of class de.iftm.dcm4che.router.plugin.PseudonymPlugin.
     */
    public void testMod2Acqdat6_Birdat6() {
        Dataset ds = null;
        String  s;

        System.out.println("testMod2Acqdat8_Birdat8");

        // Create an empty dataset
        ds = DcmObjectFactory.getInstance().newDataset();

        // Set the character set to ISO-8859-1 for Western Europe
        // Otherwise we would get an IllegalArgumentException on loading
        // a dataset with German umlauts 
        ds.putCS(Tags.SpecificCharacterSet, "ISO_IR 100");

        s = pseudonymPlugin.mod2Acqdat8_Birdat8(ds);
        assertEquals(s, "XX20030101_20000101", s);

        ds.putDA(Tags.PatientBirthDate, "19401201");
        s = pseudonymPlugin.mod2Acqdat8_Birdat8(ds);
        assertEquals(s, "XX20030101_19401201", s);

        ds.putPN(Tags.Modality, "CT");
        ds.putDA(Tags.PatientBirthDate, "19401201");
        ds.putDA(Tags.AcquisitionDate, "20030731");
        s = pseudonymPlugin.mod2Acqdat8_Birdat8(ds);
        assertEquals(s, "CT20030731_19401201", s);
    }
    
    /**
     * Test of mod2Acqdat8PatID method, of class de.iftm.dcm4che.router.plugin.PseudonymPlugin.
     */
    public void testMod2Acqdat8PatID() {
        Dataset ds = null;
        String  s;

        System.out.println("testMod2Acqdat8PatID");

        // Create an empty dataset
        ds = DcmObjectFactory.getInstance().newDataset();

        // Set the character set to ISO-8859-1 for Western Europe
        // Otherwise we would get an IllegalArgumentException on loading
        // a dataset with German umlauts 
        ds.putCS(Tags.SpecificCharacterSet, "ISO_IR 100");
        ds.putPN(Tags.PatientName, "Mustermann^Hans Werner^von^Dr.");
        ds.putDA(Tags.PatientBirthDate, "19401201");
                
        s = pseudonymPlugin.mod2Acqdat8PatID(ds);
        assertEquals(s, "UN10660401^NOID", s);

        ds.putDA(Tags.AcquisitionDate, "20030731");
        s = pseudonymPlugin.mod2Acqdat8PatID(ds);
        assertEquals(s, "UN20030731^NOID", s);

        ds.putCS(Tags.Modality, "CT");
        s = pseudonymPlugin.mod2Acqdat8PatID(ds);
        assertEquals(s, "CT20030731^NOID", s);
        
        ds.putLO(Tags.PatientID, "P31415926");
        s = pseudonymPlugin.mod2Acqdat8PatID(ds);
        assertEquals(s, "CT20030731^P31415926", s);        
    }
    
    /**
     * Test of mod2Acqdat8PatID method, of class de.iftm.dcm4che.router.plugin.PseudonymPlugin.
     */
    public void testMod2Acqdat8PatIDA() {
        Properties props = new Properties();
        props.setProperty("type", "#Mod2Acqdat8PatID");
        pseudonymPlugin.init(props);
        System.out.println("testMod2Acqdat8PatID");

        // Create an empty dataset
        Dataset ds = DcmObjectFactory.getInstance().newDataset();

        // Set the character set to ISO-8859-1 for Western Europe
        // Otherwise we would get an IllegalArgumentException on loading
        // a dataset with German umlauts 
        ds.putCS(Tags.SpecificCharacterSet, "ISO_IR 100");
        ds.putPN(Tags.PatientName, "Mustermann^Hans Werner^von^Dr.");
        ds.putDA(Tags.PatientBirthDate, "19401201");
        ds.putDA(Tags.AcquisitionDate, "20030731");
        ds.putCS(Tags.Modality, "MR");    
        ds.putLO(Tags.PatientID, "P31415926");
        ds.putCS(Tags.PatientSex, "M");    
        
        pseudonymPlugin.process(ds);
        
        assertEquals(ds.getString(0x00100010), "MR20030731^P31415926");        
    }

    /**
     * Test of DOB anonymizing method, of class de.iftm.dcm4che.router.plugin.PseudonymPlugin.
     */
    public void testDOBWithAgeAndBirthDate() {
        Properties props = new Properties();
        props.setProperty("type", "#Mod2Acqdat8PatID");
        pseudonymPlugin.init(props);
        System.out.println("testDOBWithAgeAndBirthDate");

        // Create an empty dataset
        Dataset ds = DcmObjectFactory.getInstance().newDataset();

        // Set the character set to ISO-8859-1 for Western Europe
        // Otherwise we would get an IllegalArgumentException on loading
        // a dataset with German umlauts 
        ds.putCS(Tags.SpecificCharacterSet, "ISO_IR 100");
        ds.putPN(Tags.PatientName, "Mustermann^Hans Werner^von^Dr.");
        ds.putDA(Tags.PatientBirthDate, "19401201");
        ds.putDA(Tags.AcquisitionDate, "20030731");
        ds.putDA(Tags.StudyDate, "20030731");        
        ds.putCS(Tags.Modality, "MR");    
        ds.putLO(Tags.PatientID, "P31415926");
        ds.putCS(Tags.PatientSex, "M"); 
        ds.putCS(Tags.PatientAge, "62Y");
        
        pseudonymPlugin.process(ds);
        
        assertEquals(ds.getString(0x00100030), "19410731");      
    }

    /**
     * Test of DOB anonymizing method, of class de.iftm.dcm4che.router.plugin.PseudonymPlugin.
     */
    public void testDOBWithBirthDateButNoAge() {
        Properties props = new Properties();
        props.setProperty("type", "#Mod2Acqdat8PatID");
        pseudonymPlugin.init(props);
        System.out.println("testDOBWithBirthDateButNoAge");

        // Create an empty dataset
        Dataset ds = DcmObjectFactory.getInstance().newDataset();

        // Set the character set to ISO-8859-1 for Western Europe
        // Otherwise we would get an IllegalArgumentException on loading
        // a dataset with German umlauts 
        ds.putCS(Tags.SpecificCharacterSet, "ISO_IR 100");
        ds.putPN(Tags.PatientName, "Mustermann^Hans Werner^von^Dr.");
        ds.putDA(Tags.PatientBirthDate, "19401201");
        ds.putDA(Tags.AcquisitionDate, "20030731");
        ds.putDA(Tags.StudyDate, "20030731");        
        ds.putCS(Tags.Modality, "MR");    
        ds.putLO(Tags.PatientID, "P31415926");
        ds.putCS(Tags.PatientSex, "M"); 
        
        pseudonymPlugin.process(ds);

        assertEquals(ds.getString(0x00100030), "19400731");      
    }


    /**
     * Test of process method for a phantom scan,
     * of class de.iftm.dcm4che.router.plugin.PseudonymPlugin.
     */
    public void testProcessPhantomScan() {
        Properties props = new Properties();
        props.setProperty("type", "#Mod2Acqdat8PatID");
        pseudonymPlugin.init(props);
        System.out.println("testProcessPhantomScan");

        // Create an empty dataset
        Dataset ds = DcmObjectFactory.getInstance().newDataset();

        // Set the character set to ISO-8859-1 for Western Europe
        // Otherwise we would get an IllegalArgumentException on loading
        // a dataset with German umlauts 
        ds.putCS(Tags.SpecificCharacterSet, "ISO_IR 100");
        ds.putPN(Tags.PatientName, "Gel Phantom");
        ds.putDA(Tags.PatientBirthDate, "20010101");
        ds.putDA(Tags.AcquisitionDate, "20131231");
        ds.putDA(Tags.StudyDate, "20131231");        
        ds.putCS(Tags.Modality, "MR");    
        ds.putLO(Tags.PatientID, "PQA20131231");
        ds.putCS(Tags.PatientSex, "O");

        pseudonymPlugin.process(ds);
        assertEquals("Gel Phantom", ds.getString(0x00100010));
        assertEquals("20010101", ds.getString(0x00100030));      
    }

    /**
     * Test of process method for a child scan with age in months,
     * of class de.iftm.dcm4che.router.plugin.PseudonymPlugin.
     */
    public void testProcessChildScan() {
        Properties props = new Properties();
        props.setProperty("type", "#Mod2Acqdat8PatID");
        pseudonymPlugin.init(props);
        System.out.println("testProcessChildScan");

        // Create an empty dataset
        Dataset ds = DcmObjectFactory.getInstance().newDataset();

        // Set the character set to ISO-8859-1 for Western Europe
        // Otherwise we would get an IllegalArgumentException on loading
        // a dataset with German umlauts 
        ds.putCS(Tags.SpecificCharacterSet, "ISO_IR 100");
        ds.putPN(Tags.PatientName, "Jovencita^Algo");
        ds.putDA(Tags.PatientBirthDate, "20010102");
        ds.putDA(Tags.AcquisitionDate, "20010305");
        ds.putDA(Tags.StudyDate, "20010305");        
        ds.putCS(Tags.Modality, "MR");    
        ds.putLO(Tags.PatientID, "CRIS000001");
        ds.putCS(Tags.PatientSex, "F");
        ds.putAS(Tags.PatientAge, "002M");

        pseudonymPlugin.process(ds);
        assertEquals("MR20010305^CRIS000001", ds.getString(0x00100010));
        assertEquals("20010105", ds.getString(0x00100030));      
    }   

    /**
     * Test of process method for an infant scan with age in weeks,
     * of class de.iftm.dcm4che.router.plugin.PseudonymPlugin.
     */
    public void testProcessInfantScan() {
        Properties props = new Properties();
        props.setProperty("type", "#Mod2Acqdat8PatID");
        pseudonymPlugin.init(props);
        System.out.println("testProcessInfantScan");

        // Create an empty dataset
        Dataset ds = DcmObjectFactory.getInstance().newDataset();

        // Set the character set to ISO-8859-1 for Western Europe
        // Otherwise we would get an IllegalArgumentException on loading
        // a dataset with German umlauts 
        ds.putCS(Tags.SpecificCharacterSet, "ISO_IR 100");
        ds.putPN(Tags.PatientName, "Jovencita^Muy");
        ds.putDA(Tags.PatientBirthDate, "20010102");
        ds.putDA(Tags.AcquisitionDate, "20010207");
        ds.putDA(Tags.StudyDate, "20010207");        
        ds.putCS(Tags.Modality, "MR");    
        ds.putLO(Tags.PatientID, "CRIS000001");
        ds.putCS(Tags.PatientSex, "F");
        ds.putAS(Tags.PatientAge, "005W");

        pseudonymPlugin.process(ds);
        assertEquals("MR20010207^CRIS000001", ds.getString(0x00100010));
        assertEquals("20010103", ds.getString(0x00100030));      
    }

    /**
     * Test of process method for a neonate scan with age in days,
     * of class de.iftm.dcm4che.router.plugin.PseudonymPlugin.
     */
    public void testProcessNeonateScan() {
        Properties props = new Properties();
        props.setProperty("type", "#Mod2Acqdat8PatID");
        pseudonymPlugin.init(props);
        System.out.println("testProcessNeonateScan");

        // Create an empty dataset
        Dataset ds = DcmObjectFactory.getInstance().newDataset();

        // Set the character set to ISO-8859-1 for Western Europe
        // Otherwise we would get an IllegalArgumentException on loading
        // a dataset with German umlauts 
        ds.putCS(Tags.SpecificCharacterSet, "ISO_IR 100");
        ds.putPN(Tags.PatientName, "Jovencita^Extremamente");
        ds.putDA(Tags.PatientBirthDate, "20010102");
        ds.putDA(Tags.AcquisitionDate, "20010124");
        ds.putDA(Tags.StudyDate, "20010124");        
        ds.putCS(Tags.Modality, "MR");    
        ds.putLO(Tags.PatientID, "CRIS000001");
        ds.putCS(Tags.PatientSex, "F");
        ds.putAS(Tags.PatientAge, "022D");

        pseudonymPlugin.process(ds);
        assertEquals("MR20010124^CRIS000001", ds.getString(0x00100010));
        assertEquals("20010103", ds.getString(0x00100030));      
    }  
}

