/*
 *  UnitTest: PrivateRemovePluginTest
**/
 
package de.iftm.dcm4che.router.plugin;

import junit.framework.*;
import org.apache.log4j.*;
import org.dcm4che.data.*;
import org.dcm4che.dict.*;
import java.util.*;
import java.io.*;

/**
 * JUnit based test for the class: PrivateRemovePlugin.java
 * <br>
 * @author R.Hartley-Davies
 * @version 2012.02.02
 */
public class PrivateRemovePluginTest extends TestCase {
    
    /** A link to a MR DICOM file */
    final String MR_IMAGE_URI = "MR_example.dcm";

    /** A link to a Siemens MR DICOM file */
    final String MR_SIEMENS_IMAGE_URI = "MR_siemens_example.dcm";

    /** A link to a Siemens ASL MR DICOM file */
    final String MR_ASL_SIEMENS_IMAGE_URI = "MR_ASL_Siemens_example.dcm";

    /** A link to a Siemens fMRI MR DICOM file */
    final String MR_FNL_SIEMENS_IMAGE_URI = "MR_fnl_siemens_example.dcm";

    /** A link to a Siemens DTI MR DICOM file */
    final String MR_DTI_SIEMENS_IMAGE_URI = "MR_dti_siemens_example.dcm";

    /** A link to a Siemens MRS file */
    final String MRS_SIEMENS_IMAGE_URI = "MRS_siemens_example.dcm";

    /** The class to test */
    PrivateRemovePlugin plugin = null;
    
    
    /**
     * Creates a new PseudonymPluginTest object.
     *
     * @param testName name of test
     */
    public PrivateRemovePluginTest(java.lang.String testName) {
        super(testName);
    }


    /**
     * Setup procedures.
     * This method is called once during initialization.
     */
    protected void setUp() {
        // Delete all existing appenders and set logging level to INFO
        BasicConfigurator.resetConfiguration();

        // Initialise Logging System 
        BasicConfigurator.configure();

        // Set Logging Level to INFO
        Logger.getRootLogger().setLevel(Level.INFO);

        // Create an instance of the plugin
        plugin = new PrivateRemovePlugin();

        // Set empty properties for the plugin
        plugin.init(new Properties());
    }


    /**
     * Cleanup procedures.
     */
    public void tearDown() {}


    /**
     * Main method to run the test suite.
     * @param args The argument strings.
     */
    public static void main(java.lang.String[] args) {
        junit.textui.TestRunner.run(suite());
    }


    /**
     * Create a new test suite based on this class.
     * @return The test suite
     */
    public static Test suite() {
        return new TestSuite(PrivateRemovePluginTest.class);
    }


    /**
     * Test of init method,
     * of class de.iftm.dcm4che.router.plugin.PrivateRemovePlugin.
     */
    public void testInitProperties() {
        String value;
        String contents;
        Properties props = new Properties();
        
        System.out.println("testInitProperties");        
        value = "True";
        props.setProperty("AllowUnlisted", value);
        plugin.init(props);
        assertEquals(true, plugin.allow_unlisted);
        
        value = "@00A1";
        props.setProperty("AllowedGroups", value);
        plugin.init(props);
        assertEquals(1, plugin.allowed_groups.size());
        assertEquals(161, (int)plugin.allowed_groups.get(0));
    }


    public void testGetGroupList() {
        String s0 = "";
        List<Integer> l0 = plugin.getGroupList(s0);
        assertEquals(0, l0.size());
        
        String s1 = "@00A10000";
        List<Integer> l1 = plugin.getGroupList(s1);
        assertEquals(1, l1.size());
        assertEquals(161, (int)l1.get(0));

        String s2 = "@00A10000, @00B11234";
        List<Integer> l2 = plugin.getGroupList(s2);
        assertEquals(2, l2.size());
        assertEquals(161, (int)l2.get(0));
        assertEquals(177, (int)l2.get(1));   
        
        String s3 = "@00A1";
        List<Integer> l3 = plugin.getGroupList(s3);
        assertEquals(1, l3.size());
        assertEquals(161, (int)l3.get(0));
        
        String s4 = "@FFFF";
        List<Integer> l4 = plugin.getGroupList(s4);
        assertEquals(0, l4.size());

        String s5 = "@0000";
        List<Integer> l5 = plugin.getGroupList(s5);
        assertEquals(0, l5.size());
    }

    
    public void testGetGroupFromPropertyString() {
        String s0 = "";
        int g0 = plugin.getGroupFromPropertyString(s0);
        assertEquals(g0, -1);
        
        String s1 = "@00C30000";
        int g1 = plugin.getGroupFromPropertyString(s1);
        assertEquals(g1, 0x00c3);

        String s2 = "@f000";
        int g2 = plugin.getGroupFromPropertyString(s2);
        assertEquals(g2, -1);
        
        String s3 = "@0000";
        int g3 = plugin.getGroupFromPropertyString(s3);
        assertEquals(g3, -1);
        
        String s4 = "@7123";
        int g4 = plugin.getGroupFromPropertyString(s4);
        assertEquals(g4, 0x7123);             
    }

    
    public void testProcessDefault() throws Exception {
        Properties props = new Properties();
        plugin.init(props);
        Dataset ds = DcmObjectFactory.getInstance().newDataset();
        // private creator element reserving block 00777700-007777ff
        ds.putLO(0x00777700, "**Forbiddden Private Creator A**");
        // element in private block
        ds.putLO(0x00777710, "**Forbidden Private Tag A**");
        // private creator element reserving block 00797700-007977ff
        ds.putLO(0x00797700, "**Forbiddden Private Creator A**");
        // element in private block
        ds.putLO(0x00797712, "**Forbidden Private Tag A**");
        // unknown element in unknown public group
        ds.putLO(0x40024002, "**Forbidden Unlisted A**");
        // unknown element in known public group
        ds.putLO(0x30021000, "**Forbidden Unlisted B**");
        
        assertEquals(6, ds.size()); 
                
        plugin.process(ds);
        
        // should have dropped everything       
        assertEquals(0, ds.size());                 
    }


    public void testProcessAllowUnlisted() throws Exception {
        Properties props = new Properties();
        props.setProperty("AllowUnlisted", "True");
        plugin.init(props);
        Dataset ds = DcmObjectFactory.getInstance().newDataset();        
        // private creator element reserving block 00777700-007777ff
        ds.putLO(0x00777700, "**Forbiddden Private Creator A**");
        // element in private block
        ds.putLO(0x00777710, "**Forbidden Private Tag A**");
        // private creator element reserving block 00797700-007977ff
        ds.putLO(0x00797700, "**Forbiddden Private Creator B**");
        // element in private block
        ds.putLO(0x00797712, "**Forbidden Private Tag B**");
        // unknown element in unknown public group
        ds.putLO(0x40024002, "Allowed Unlisted A");
        // unknown element in known public group
        ds.putLO(0x30021000, "Allowed Unlisted B");
        
        assertEquals(6, ds.size()); 
                
        plugin.process(ds);
        
        // should have left the last two       
        assertEquals(2, ds.size());      
        assertEquals(null, ds.getString(0x00777700));
        assertEquals(null, ds.getString(0x00777710));
        assertEquals(null, ds.getString(0x00797700));
        assertEquals(null, ds.getString(0x00797712));
        assertEquals("Allowed Unlisted A", ds.getString(0x40024002));
        assertEquals("Allowed Unlisted B", ds.getString(0x30021000));
    }


    public void testProcessAllowedGroups() throws Exception {
        Properties props = new Properties();
        props.setProperty("AllowedGroups", "@0077, @0079");
        plugin.init(props);
        Dataset ds = DcmObjectFactory.getInstance().newDataset();        
        // private creator element reserving block 00777700-007777ff
        ds.putLO(0x00777700, "Allowed Private Creator A");
        // element in private block
        ds.putLO(0x00777710, "Allowed Private Tag A");
        // private creator element reserving block 00797700-007977ff
        ds.putLO(0x00797700, "Allowed Private Creator B");
        // element in private block
        ds.putLO(0x00797712, "Allowed Private Tag B");
        // unknown element in unknown public group
        ds.putLO(0x40024002, "**Forbidden Unlisted A**");
        // unknown element in known public group
        ds.putLO(0x30021000, "**Forbidden Unlisted B**");
        
        assertEquals(6, ds.size()); 
                
        plugin.process(ds);
        
        // should have left the first four       
        assertEquals(4, ds.size());
        assertEquals("Allowed Private Creator A", ds.getString(0x00777700));
        assertEquals("Allowed Private Tag A", ds.getString(0x00777710));
        assertEquals("Allowed Private Creator B", ds.getString(0x00797700));
        assertEquals("Allowed Private Tag B", ds.getString(0x00797712));
        assertEquals(null, ds.getString(0x40024002));
        assertEquals(null, ds.getString(0x30021000));
    }

    
    public void testProcessMRExample() throws Exception {
        Properties props = new Properties();
        plugin.init(props);
        Dataset ds = DcmObjectFactory.getInstance().newDataset();
        File f = new File("samples", MR_IMAGE_URI);
        InputStream in = new FileInputStream(f);
        ds.readFile(in, FileFormat.DICOM_FILE, 0x7fff0000);
        assertEquals(109, ds.size());                
        plugin.process(ds);
        // Example has no private tags
        assertEquals(109, ds.size());
        //ds.dumpDataset(System.out, null);     
    }


    public void testProcessMRASLSiemensExample() throws Exception {
        Properties props = new Properties();
        plugin.init(props);
        Dataset ds = DcmObjectFactory.getInstance().newDataset();
        File f = new File("samples", MR_ASL_SIEMENS_IMAGE_URI);
        InputStream in = new FileInputStream(f);
        ds.readFile(in, FileFormat.DICOM_FILE, 0x7fff0000);
        assertEquals(147, ds.size());                
        plugin.process(ds);
        assertEquals(110, ds.size());

        // test to check groups 19,29,51 all removed
        assertEquals(null, ds.getString(0x00190010));
        assertEquals(null, ds.getString(0x00290010));       
        assertEquals(null, ds.getString(0x00510010));
                
        props.setProperty("AllowedGroups", "@0019, @0029");        
        plugin.init(props);
        ds = DcmObjectFactory.getInstance().newDataset();        
        f = new File("samples", MR_ASL_SIEMENS_IMAGE_URI);
        in = new FileInputStream(f);
        ds.readFile(in, FileFormat.DICOM_FILE, 0x7fff0000);
        plugin.process(ds);
        assertEquals(131, ds.size());
        //ds.dumpDataset(System.out, null);
        
        // test to check groups 19,29 ok but 51 removed
        assertEquals("SIEMENS MR HEADER", ds.getString(0x00190010));
        assertEquals("IMAGE NUM 4", ds.getString(0x00191008));

        assertEquals("SIEMENS CSA HEADER", ds.getString(0x00290010));
        assertEquals("SIEMENS MEDCOM HEADER2", ds.getString(0x00290011));
        assertEquals("IMAGE NUM 4", ds.getString(0x00291008));                   
        assertEquals(null, ds.getString(0x00510010));          
    }
}


