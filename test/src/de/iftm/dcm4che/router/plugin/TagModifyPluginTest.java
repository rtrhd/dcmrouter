/*
 *  UnitTest: TagModifyPluginTest
**/
 
package de.iftm.dcm4che.router.plugin;

import junit.framework.*;
import org.apache.log4j.*;
import org.dcm4che.data.*;
import org.dcm4che.dict.*;
import java.util.*;
import java.io.*;

/**
 * JUnit based test for the class: TagModifyPlugin.java
 * <br>
 * @author R.Hartley-Davies
 * @version 2012.02.03
 */
public class TagModifyPluginTest extends TestCase {
    
    /** A link to a MR DICOM file */
    final String MR_IMAGE_URI = "MR_example.dcm";

    /** A link to a Siemens MR DICOM file */
    final String MR_SIEMENS_IMAGE_URI = "MR_siemens_example.dcm";

    /** A link to a Siemens ASL MR DICOM file */
    final String MR_ASL_SIEMENS_IMAGE_URI = "MR_ASL_Siemens_example.dcm";

    /** A link to a Siemens fMRI MR DICOM file */
    final String MR_FNL_SIEMENS_IMAGE_URI = "MR_fnl_siemens_example.dcm";

    /** A link to a Siemens DTI MR DICOM file */
    final String MR_DTI_SIEMENS_IMAGE_URI = "MR_dti_siemens_example.dcm";

    /** A link to a Siemens MRS file */
    final String MRS_SIEMENS_IMAGE_URI = "MRS_siemens_example.dcm";

    /** The class to test */
    TagModifyPlugin plugin = null;
    
    
    /**
     * Creates a new TagModifyPluginTest object.
     *
     * @param testName name of test
     */
    public TagModifyPluginTest(java.lang.String testName) {
        super(testName);
    }


    /**
     * Setup procedures.
     * This method is called once during initialization.
     */
    protected void setUp() {
        // Delete all existing appenders and set logging level to INFO
        BasicConfigurator.resetConfiguration();

        // Initialise Logging System 
        BasicConfigurator.configure();

        // Set Logging Level to INFO
        Logger.getRootLogger().setLevel(Level.INFO);

        // Create an instance of the plugin
        plugin = new TagModifyPlugin();

        // Set empty properties for the plugin
        plugin.init(new Properties());
    }


    /**
     * Cleanup procedures.
     */
    public void tearDown() {}


    /**
     * Main method to run the test suite.
     * @param args The argument strings.
     */
    public static void main(java.lang.String[] args) {
        junit.textui.TestRunner.run(suite());
    }


    /**
     * Create a new test suite based on this class.
     * @return The test suite
     */
    public static Test suite() {
        return new TestSuite(TagModifyPluginTest.class);
    }


    /**
     * Test of init method,
     * of class de.iftm.dcm4che.router.plugin.TagModifyPlugin.
     */
    public void testInitProperties() {
        // The init method should just store the properties
        Properties props = new Properties();
        plugin.init(props);
        assertEquals(props, plugin.props);
    }


    
    public void testProcessClear() throws Exception {
        Properties props = new Properties();
        Dataset ds = DcmObjectFactory.getInstance().newDataset();
        
        ds.putDA(0x00100030, "10660401");
        props.setProperty("@00100030", "#clear"); // Pat Birthdate

        ds.putCS(0x00080060, "MR");
        props.setProperty("$Modality", "#clear"); // Modality

        ds.putLO(0x00080070, "Siemens");

        assertEquals(3, ds.size());      
        assertEquals("10660401", ds.getString(0x00100030));
        assertEquals("MR", ds.getString(0x00080060));
        assertEquals("Siemens", ds.getString(0x00080070));
                
        plugin.init(props);
        plugin.process(ds);

        assertEquals(3, ds.size());
        // a bit odd - would expect "" but seems to be the way the api works    
        assertEquals(null, ds.getString(0x00100030));
        assertEquals(null, ds.getString(0x00080060));
        assertEquals("Siemens", ds.getString(0x00080070));
    }


    public void testProcessRemove() throws Exception {
        Properties props = new Properties();
        Dataset ds = DcmObjectFactory.getInstance().newDataset();
        
        ds.putDA(0x00100030, "10660401");
        props.setProperty("@00100030", "#remove"); // Pat Birthdate

        ds.putCS(0x00080060, "MR");
        props.setProperty("$Modality", "#remove"); // Modality

        ds.putLO(0x00080070, "Siemens");

        assertEquals(3, ds.size());      
        assertEquals("10660401", ds.getString(0x00100030));
        assertEquals("MR", ds.getString(0x00080060));
        assertEquals("Siemens", ds.getString(0x00080070));
                
        plugin.init(props);
        plugin.process(ds);

        assertEquals(1, ds.size());      
        assertEquals(null, ds.getString(0x00100030));
        assertEquals(null, ds.getString(0x00080060));
        assertEquals("Siemens", ds.getString(0x00080070));
    }

    public void testProcessReplace() throws Exception {
        Properties props = new Properties();
        Dataset ds = DcmObjectFactory.getInstance().newDataset();

        ds.putDA(0x00080020, "19991231"); // Study date               
        ds.putDA(0x00100030, "10660401");
        props.setProperty("@00100030", "@00080020"); // Pat Birthdate

        ds.putCS(0x00080060, "MR");
        props.setProperty("$Modality", "CT"); // Modality

        ds.putLO(0x00080070, "Siemens");

        assertEquals(4, ds.size());
        assertEquals("19991231", ds.getString(0x00080020));        
        assertEquals("10660401", ds.getString(0x00100030));
        assertEquals("MR", ds.getString(0x00080060));
        assertEquals("Siemens", ds.getString(0x00080070));
                
        plugin.init(props);
        plugin.process(ds);

        assertEquals(4, ds.size());
        assertEquals("19991231", ds.getString(0x00080020));        
        assertEquals("19991231", ds.getString(0x00100030));
        assertEquals("CT", ds.getString(0x00080060));
        assertEquals("Siemens", ds.getString(0x00080070));
    }
}


