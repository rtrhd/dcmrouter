/*
 *  PseudonymPlugin.java
 *
 *  Copyright (c) 2003 by
 *
 *  IFTM Institut fuer Telematik in der Medizn GmbH
 *  VISUS Technology Transfer GmbH
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 *****************************************************************************/
package de.iftm.dcm4che.router.plugin;

import de.iftm.dcm4che.router.property.DicomRouterProperties;
import org.apache.log4j.*;
import org.dcm4che.data.*;
import org.dcm4che.dict.*;
import java.util.*;
import java.text.SimpleDateFormat;

/**
 * This plugin replaces the PatientName with a pseudonym. The plugin supports
 * the properties which start with the key PseudonymPlugin and have the subkeys:<br>
 * <br>
 * type<br>
 * The key may have one of the following predefined values:<br>
 * #Initials2_Birdat4<br>
 * Creates a new Patientname: ii_bbbbbb with<br>
 * i the initials of the patient: name surename<br>
 * b patient birth date<br>
 * <br>
 * #Mod2Acqdat8_Birdat8<br>
 * Creates a new Patientname: mmaaaaaaaa_bbbbbbbb with<br>
 * m modality<br>
 * a acquisition date<br>
 * b patient birth date<br>
 * <br>
 * #Mod2Acqdat8PatID<br>
 * Creates a new Patientname: mmaaaaaaaa<patid> with<br>
 * m modality<br>
 * a acquisition date<br>
 * <br>
 * Default value: #Mod2Acqdat8PatID<br>
 *
 * @author Thomas Hacklaender
 * @version 2003.11.14
 */
public class PseudonymPlugin extends DicomRouterPlugin {
    /** Enumerated values of the types of pseudonym. */
    public enum PseudonymType {
        INITIALS2_BIRDAT6,
        MOD2ACQDAT8_BIRDAT8,
        MOD2ACQDAT8PATID
    }

    /** The version string */
    final String VERSION = "2013.01.15";
    
    /** The logger for this plugin */
    Logger log = Logger.getLogger(PseudonymPlugin.class);
    
    /** The properties of this plugin */
    Properties props = null;
    
    /** Type of pseudonym (default) */
    PseudonymType ptype = PseudonymType.MOD2ACQDAT8PATID;
    
    /** Constructor */
    public PseudonymPlugin() {}


    /**
     * Initializes the DicomRouterPlugin with the specified Properties.
     * @param p Properties containing the configuration of the plugin
     */
    public void init(Properties p) {
        String s;
        
        // Properties lokal speichern
        props = p;
        
        // Art der Pseudonymisierung festlegen
        s = props.getProperty("type");        
        if(s != null) {
            if(s.equals("#Mod2Acqdat8_Birdat8")) {
                ptype = PseudonymType.MOD2ACQDAT8_BIRDAT8;
            } else if(s.equals("#Initials2_Birdat6")) {
                ptype = PseudonymType.INITIALS2_BIRDAT6;
            } else if(s.equals("#Mod2Acqdat8PatID")) {
                ptype = PseudonymType.MOD2ACQDAT8PATID;
            } else {
                log.warn("Unknown pseudonym type in properties: " + s +
                        ". Replaced by default #Mod2Acqdat8PatID");
            }
        }
    }

   
    /**
     * Returns a version string.
     *
     * @return The version string
     */
    public String getVersion() {
        return VERSION;
    }

    
    /**
     * Closes the Plugin.
     */
    public void close() {}
    
    /**
     * Contains the processing of this plugin. This implementation handles all
     * exceptions inside the method and sends logging information about the exception.
     * 
     * @param dataset The Dataset to process.
     * @return If successful CONTINUE, REQUEST_ABORT_RECEIVER otherwise
     */
    protected int process(Dataset dataset) {
        String originalName;
        String newName = "Pseudonym";
        PersonName pn;
        
        // Check we have a dataset
        if(dataset == null) {
            log.warn("No Dataset given.");          
            return REQUEST_ABORT_RECEIVER;
        }

        originalName = dataset.getString(Tags.PatientName);

        // Leave phantom images unmodified
        String patientID  = dataset.getString(Tags.PatientID);
        if(patientID != null)
            patientID = patientID.toLowerCase();
        String patientSex = dataset.getString(Tags.PatientSex);
        if(patientSex != null)
            patientSex = patientSex.toLowerCase();

        if(patientSex != null && patientSex.startsWith("o")) {
            if(patientID != null &&
               !patientID.startsWith("ra") &&
               !patientID.startsWith("t")  &&
               !patientID.startsWith("cris")) {
                if(log.isInfoEnabled()) {
                    log.info("PatientName " + originalName + " unaltered as it is a phantom scan" );
                }
                return CONTINUE;
            }
        }

        switch(ptype) {
            case INITIALS2_BIRDAT6:
                newName = initials2_Birdat6(dataset);                
                break;
                
            case MOD2ACQDAT8_BIRDAT8:
                newName = mod2Acqdat8_Birdat8(dataset);                
                break;

            case MOD2ACQDAT8PATID:
                newName = mod2Acqdat8PatID(dataset);                
                break;
        }
        
        pn = DcmObjectFactory.getInstance().newPersonName(newName);
        dataset.putPN(Tags.PatientName, pn);
        
        if(log.isInfoEnabled()) {
            log.info("PatientName " + originalName + " replaced by: " + newName);
        }

        // Replace Birthdate with a date age years before scan
        Date originalBirthDate = dataset.getDate(Tags.PatientBirthDate);
        Date newBirthDate = anonymousBirthDay(dataset);
        dataset.putDA(Tags.PatientBirthDate, newBirthDate);
        
        if(log.isInfoEnabled()) {
            String olddob = new SimpleDateFormat("yyyyMMdd").format(originalBirthDate);
            String newdob = new SimpleDateFormat("yyyyMMdd").format(newBirthDate);            
            log.info("PatientDOB " + olddob + " replaced by: " + newdob);
        }

        return CONTINUE;
    }
    

    /**
     * Creates a new Patient Birth Date based on the Study date and the Patient Age.
     * @param ds the Dataset to process
     * @note RHD
     * @return a Date object for the anonymized date of birth
     */
    protected Date anonymousBirthDay(Dataset ds) {
        // Get the study date
        Date studyDate = ds.getDate(Tags.StudyDate);
        Date acqDate = ds.getDate(Tags.AcquisitionDate);
        if(studyDate == null && acqDate != null) {
            studyDate = acqDate;
        }
        if(studyDate == null) {
            studyDate = new Date();
            log.warn("Can't find StudyDate tag. Using today's date");
        }

        // and try and offset it back by the patient age.
        // (this is an unbelievable can of worms in Java
        // it will still break across year boundaries etc)
        String age = ds.getString(Tags.PatientAge);
        Date birthDate = ds.getDate(Tags.PatientBirthDate);  
        if(age == null && birthDate != null) {
            log.info("Missing age field. Deducing from birthdate.");
            Calendar bdatecal = Calendar.getInstance();
            bdatecal.setTime(birthDate);
            bdatecal.set(Calendar.MILLISECOND, 0);
            Calendar studatecal = Calendar.getInstance();            
            studatecal.setTime(studyDate);
            bdatecal.set(Calendar.MILLISECOND, 0);
            int yeardiff = studatecal.get(Calendar.YEAR) - bdatecal.get(Calendar.YEAR);
            if(yeardiff>0) {
                age = Integer.toString(yeardiff) + "Y";
            } else {
                int monthdiff = studatecal.get(Calendar.MONTH) - bdatecal.get(Calendar.MONTH);
                if(monthdiff>0) {                
                    age = Integer.toString(monthdiff) + "M";
                } else {
                    int daydiff = studatecal.get(Calendar.DAY_OF_MONTH) - bdatecal.get(Calendar.DAY_OF_MONTH);
                    age = Integer.toString(daydiff) + "D";
                }
            }
        }
        if(age == null) {
            log.warn("Unable to parse get Patient age. Using 111 years");
            age = "111Y";
        }

        String ageUnit = age.substring(age.length()-1).toUpperCase();
        int ageNumber;
        try {
            ageNumber = Integer.parseInt(age.substring(0, age.length()-1));
        } catch(NumberFormatException e) {
            ageNumber = -1;
        }        

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(studyDate);

        if(ageUnit.equals("Y") && 0<=ageNumber && ageNumber<=120) {
            calendar.add(Calendar.YEAR, -ageNumber);
        } else if(ageUnit.equals("M") && 0<=ageNumber && ageNumber<=60) {
            calendar.add(Calendar.MONTH, -ageNumber);
        } else if(ageUnit.equals("W") && 0<=ageNumber && ageNumber<=52) {
            calendar.add(Calendar.DAY_OF_MONTH, -ageNumber*7);
        } else if(ageUnit.equals("D") && 0<=ageNumber && ageNumber<=48) {
            // we can't keep this consistent and still anonymise
            // so we'll force it to the nearest week
            calendar.add(Calendar.DAY_OF_MONTH, -((ageNumber + 4) / 7) * 7);
        } else {
            calendar.add(Calendar.YEAR, -111);
            log.warn("Unable to parse Patient age tag. Using 111 years");
        }
        return calendar.getTime();
    }
    
    
    /**
     * Creates a new Patientname: ii_bbbbbb with<br>
     * i the initials of the patient: name surename<br>
     * b patient birth date<br>
     * @param ds the Dataset to process
     *
     * @return the patientname
     */
    protected String initials2_Birdat6(Dataset ds) {
        int i;
        String pseudonym = "";
        String patientName;
        String patientBirthDate;
        
        // C.7.1.1 Patient Module
        if((patientName = ds.getString(Tags.PatientName)) == null) {
            patientName = "X^X";
            log.warn("Can't find PatientName tag. Replaced by: X^X");
        }
        
        if((patientBirthDate = ds.getString(Tags.PatientBirthDate)) == null) {
            patientBirthDate = "20000101";
            log.warn("Can't find PatientBirthDate tag. Replaced by: 20000101");
        }
        
        pseudonym += patientName.substring(0, 1);
        i = patientName.indexOf('^');
        
        if((i != -1) & (patientName.length() > i)) {
            pseudonym += patientName.substring(i + 1, i + 2);
        } else {
            pseudonym += "_";
        }
        
        pseudonym += ("_" + patientBirthDate.substring(2, 8));
        
        return pseudonym;
    }
 
    
    /**
     * Creates a new Patientname: mmaaaaaaaa_bbbbbbbb with<br>
     * m modality<br>
     * a acquisition date<br>
     * b patient birth date<br>
     * @param ds the Dataset to process
     *
     * @return the patientname
     */
    protected String mod2Acqdat8_Birdat8(Dataset ds) {
        String modality;
        String acquisitionDate;
        String patientBirthDate;
        
        if((modality = ds.getString(Tags.Modality)) == null) {
            modality = "XX";
            log.warn("Can't find Modality tag. Replaced by: XX");
        }
        
        if((acquisitionDate = ds.getString(Tags.AcquisitionDate)) == null) {
            acquisitionDate = "20030101";
            log.warn("Can't find AcquisitionDate tag. Replaced by: 20030101");
        }
        
        if((patientBirthDate = ds.getString(Tags.PatientBirthDate)) == null) {
            patientBirthDate = "20000101";
            log.warn("Can't find PatientBirthDate tag. Replaced by: 20000101");
        }
        
        return modality + acquisitionDate + "_" + patientBirthDate;
    }


    /**
     * Creates a new Patientname: mmpatid<br>
     * m modality<br>
     * @param ds the Dataset to process
     *
     * @return the patientname
     */
    protected String mod2Acqdat8PatID(Dataset ds) {
        String modality;
        String acquisitionDate;
        String patientID;

        if((acquisitionDate = ds.getString(Tags.AcquisitionDate)) == null) {
            acquisitionDate = "10660401";
            log.warn("Can't find AcquisitionDate tag. Replaced by: 10660401");
        }

        if((modality = ds.getString(Tags.Modality)) == null) {
            modality = "UN";
            log.warn("Can't find Modality tag. Replaced by: UN");
        }

        if((patientID = ds.getString(Tags.PatientID)) == null) {
            patientID = "NOID";
            log.warn("Can't find PatientID tag. Replaced by: NOID");
        }
        
        return  modality + acquisitionDate + "^" + patientID;
    }
}
