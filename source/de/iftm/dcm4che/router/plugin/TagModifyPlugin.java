/*
 *  TagModifyPlugin.java
 *
 *  Copyright (c) 2003 by
 *
 *  IFTM Institut fuer Telematik in der Medizn GmbH
 *  VISUS Technology Transfer GmbH
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *****************************************************************************/
package de.iftm.dcm4che.router.plugin;

import de.iftm.dcm4che.router.property.DicomRouterProperties;
import de.iftm.dcm4che.router.util.*;
import org.apache.log4j.*;
import org.dcm4che.data.*;
import org.dcm4che.dict.*;
import java.util.*;


/**
 * This plugin modifies named elements of the submitted Dataset. It
 * supports the properties which start with the key <span
 * style="font-style: italic;">TagModifyPlugin </span>and  a subkeys,
 * which is the name of a DICOM element as given in  the Tag-dictionary.
 * The name may start with the escape-character '$',  which is ignored.<br>
 * The properties have no default values.<br>
 * <br>
 * If the value of the property starts with the character '#' the&nbsp;
 * value is interpreted as a command. Supported command are:<br>
 * <br>
 * <span style="font-style: italic;"> #remove</span><br>
 * The named element is removed from the dataset.<br>
 * <br>
 * <span style="font-style: italic;"> #clear</span><br>
 * The contents of the named element is cleared.<br>
 * <br>
 * <br>
 * If the value of the property starts with the character '$' the value is
 * interpreted as a name of a DICOM element. The contents of the element
 * named in the key is replaced by the contents of the element named in
 * the value field.<br>
 * <br>
 * If the value of the property does not starts with the character&nbsp;
 * '#', '@' or '$' its contents is replaced by the value string.<br>
 * <br>
 * @author Thomas Hacklaender
 * @version 2005.01.27
 */
public class TagModifyPlugin extends DicomRouterPlugin {
    /** The version string */
    final String VERSION = "2013.01.28";
    
    /** The logger for this plugin */
    Logger log = Logger.getLogger(TagModifyPlugin.class);
    
    /** The properties of this plugin */
    Properties props = null;
    
    /** Constructor */
    public TagModifyPlugin() {}

    
    /**
     * Initialize the TagModifyPlugin with the specified Properties.
     * @param p Properties containing the configuration of the plugin
     */
    public void init(Properties p) {
        // Take a local copy of properties and do all the processing of
        // them in the process() method as the actions depend on the dataset.
        props = p;
    }
    
    
    /** Returns a version string */
    public String getVersion() {
        return VERSION;
    }

    
    /** Closes the Plugin. */
    public void close() {}


    /**
     * Contains the proccesing of this plugin. This implementation handles all
     * exceptions inside the method and sends logging information about the exception.
     * 
     * @param dataset The dicom Dataset to process.
     * @return If successful then CONTINUE, else REQUEST_ABORT_RECEIVER
     */
    protected int process(Dataset dataset) {
        // Check we have a dataset
        if(dataset == null) {
            log.warn("No Dataset given."); 
            return REQUEST_ABORT_RECEIVER;
        }
        
        // Process all the properties
        for(Enumeration p=props.propertyNames(); p.hasMoreElements();) {
            String key   = (String)p.nextElement();
            String value = props.getProperty(key);
            // RHD: this seems to be getting passed down and causing an error
            if(key.equals("name")) {
                continue;
            }
   
            int tag = Util.getTagFromPropertyString(key);
            if(tag <= 0) {
                log.error("Can't find tag (1) in property key: " + key);
                return REQUEST_ABORT_RECEIVER;
            }
            
            // The operation on the tag is specified in the value of the property
            if(value.charAt(0) == '#') {                
                // Commands begin with a hash
                if(value.equals("#clear")) {
                    // First remove then replace with an empty tag (why?)
                    // do we need to know the VR?
                    dataset.remove(tag);                    
                    dataset.putXX(tag);                   
                    if(log.isInfoEnabled()) {
                        log.info("Value of tag " + " (" + Tags.toHexString(tag, 8) + ") cleared.");
                    }
                } else if(value.equals("#remove")) {
                    dataset.remove(tag);                    
                    if(log.isInfoEnabled()) {
                        log.info("Tag " + " (" + Tags.toHexString(tag, 8) + ") removed.");
                    }
                } else {
                    log.warn("Unknown command: " + value);
                    return REQUEST_ABORT_RECEIVER;
                }               
            } else {
                // Non hash fields are replacement values, either literal or references to other tags            
                String s = Util.getTagStringOrValue(value, dataset);
                dataset.putXX(tag, s);            
                if (log.isInfoEnabled()) {
                    log.info("Value of tag " + " (" + Tags.toHexString(tag, 8) + ") replaced with " + value);
                }            
            }
        }
        return CONTINUE;
    }
}

