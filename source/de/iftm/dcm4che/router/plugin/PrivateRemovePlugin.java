/*
 *  PrivateRemovePlugin.java
 *
 *  Copyright (c) 2003 by
 *
 *  IFTM Institut fuer Telematik in der Medizin GmbH
 *  VISUS Technology Transfer GmbH
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *****************************************************************************/

package de.iftm.dcm4che.router.plugin;

import de.iftm.dcm4che.router.property.DicomRouterProperties;
import de.iftm.dcm4che.router.util.*;
import org.apache.log4j.*;
import org.dcm4che.data.*;
import org.dcm4che.dict.*;
import java.util.*;


/**
 * This removes all private elements of the submitted Dataset. Elements are
 * private if a) the group number is odd or b) if the element is not found in
 * the dictionary.<br>
 * <br>
 * It supports no properties.
 *
 * @author Thomas Hacklaender
 * @version 2003.11.14
 */
public class PrivateRemovePlugin extends DicomRouterPlugin {
    /** The version string */
    final String VERSION = "2012.02.01";

    /** The logger for this plugin */
    Logger log = Logger.getLogger(PrivateRemovePlugin.class);

    /** The properties of this plugin */
    Properties props = null;
    
    /** Allowed Groups */
    List<Integer> allowed_groups = null;
     
    /** Don't check in dictionary for unlisted tags */
    boolean allow_unlisted = false;
    
    /** Constructor. */
    public PrivateRemovePlugin() {
        // Nothing to do.
    }


    public int getGroupFromPropertyString(String s) {
        if(s.length()<1 || s.charAt(0) != '@')
            return -1;
        try {
            int group = Integer.parseInt(s.substring(1), 16);
            if(group>0xffff)
                group >>>= 16;
            if(group>0 && group<0x7fff)
		        return group;

        } catch(NumberFormatException e) {}
        return -1;
    }
           
        
    /** Parse out a comma separated list of group numbers */
    public List<Integer> getGroupList(String s) {
        List<Integer> groups = new ArrayList<Integer>();
        StringTokenizer stk = new StringTokenizer(s, ", ");
        while(stk.hasMoreTokens()) {
            String tok = stk.nextToken();
            int group = getGroupFromPropertyString(tok);
		    if(group>=0)
		        groups.add(group);      
        }
        return groups;
    }
        

    /**
     * Initilaizes the DicomRouterPlugin with the specified Properties.
     * @param p  Properties containing the configuration of the plugin
     */
    public void init(Properties p) {
        allowed_groups = getGroupList(p.getProperty("AllowedGroups", ""));
        String allowstring = p.getProperty("AllowUnlisted", "false");
        if(allowstring.equals("True") || allowstring.equals("true") || allowstring.equals("T"))
            allow_unlisted = true;
        else
            allow_unlisted = false;
    }

    /**
     * Returns a version string.
     *
     * @return The version string
     */
    public String getVersion() {
        return VERSION;
    }
    
    /**
     * Closes the Plugin.
     */
    public void close() {
        // Nothing to do.
    }

    /**
     * Contains the proccesing of this plugin. This implementation handles all
     * exceptions inside the method and sends logging information about the exeption.
     * 
     * @param dataset The Dataset to process.
     * @return If succesful CONTINUE, REQUEST_ABORT_RECEIVER otherwise
     */
    protected int process(Dataset dataset) {
        TagDictionary dict = DictionaryFactory.getInstance()
                                              .getDefaultTagDictionary();

        // Continue only if there is a dataset
        if(dataset == null) {
            log.warn("No Dataset given.");
            return REQUEST_ABORT_RECEIVER;
        }

        // Copy the original dataset and iterate over that
        // while modifying the passed in dataset in place
        Dataset dataset_copy = DcmObjectFactory.getInstance().newDataset();
        dataset_copy.putAll(dataset);

        // Edit all elements of the dataset
        for(Iterator iter = dataset_copy.iterator(); iter.hasNext();) {
            // Get the element
            DcmElement element = (DcmElement)iter.next();
            int tagGroup = element.tag() >>> 16;
            int tagElement = element.tag() & 0xffff;

            if(allowed_groups.contains(tagGroup))
                continue;
             
            if((tagGroup % 2) == 1) {
                // 1. All Elements with an odd group number are always private
                dataset.remove(element.tag());
                log.info("Private Element (0x" + Integer.toHexString(tagGroup) + ", 0x" + Integer.toHexString(tagElement) + ") removed as group odd.");
                continue;
            }
            
            if(allow_unlisted)
                continue;
            
            if(dict.lookup(element.tag()) == null) {
                // 2. All Elemente not in the Dictionary are considered private
                dataset.remove(element.tag());
                log.info("Private Element (0x" + Integer.toHexString(tagGroup) + ", 0x" + Integer.toHexString(tagElement) + ") removed as unlisted.");
                continue;
            }
        }

        // Plugin exits without serious errors
        return CONTINUE;
    }
}
