//
// Top level cmdline prog for dcmrouter with controlled shutdown on interrupt
//
//

import de.iftm.dcm4che.router.receiver.*;

public class CmdLineRouter {
    public static void main(String[] args) {
        //System.out.println("Dicom Router starting..");
        final DicomStorageSCPReceiver dcmscp = new DicomStorageSCPReceiver(args);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                //System.out.println("Dicom Router shutting down..");
                dcmscp.stop();
            }
        });
    }
}
